import string
from random import random
import zipfile

PASSWORD_LENGTH = 4


def extract_archive(file_to_open, password):
    try:
        file_to_open.extractall(pwd=password.encode())
        return True

    except Exception as e:
        print(e)
        return False


def hack_archive(file_name):
    file_to_open = zipfile.ZipFile(file_name)  # объект архива
    wrong_passwords = []  # список паролей, которые не подошли
    tries = 0  # колличество неудачных попыток

    while True:
        password = str(random())
        extract_archive(file_to_open, password)
        tries = len(wrong_passwords)
    return password




    print(f'Archive {file_name} is hacked. Password - {password}')
    print(f'Password was found after {tries} tries')



file_name = 'task.zip'
hack_archive(file_name)

#password:0987