"""
1. Создать txt файл, в нем написать текст.
 - Считать данные с файла и вывести в консоль
 - Считать данные с этого же файла и записать в новый файл
 - Считать данные с этого же файла, преобразовать (любые операции) и записать в этот же файл с разделителем
   (пробел, точка, запятая и тп)
"""
def write_this_file():
    with open('new_text.txt', 'w+') as new_file, open('new_text.txt', 'r+') as f:
        new_file.write('hello!!!')
        file_info = f.read()
        print(file_info)


write_this_file()
def open_file():
    with open('new_text.txt', 'r+') as f:
        file_info = f.read()
        print(file_info)
open_file()
def rewrite_file():
    with open('new_new_text.txt', 'w') as n_file, open('new_text.txt', 'r') as o_file:
        n_file.write(o_file.read())
rewrite_file()

def modific_file():
    with open('new_text.txt', 'a+') as nw_file:
        nw_file.write('\n your answer is' )
        for u in nw_file:
            u = nw_file.split(',')

modific_file()
"""
2. Преобразовать дату. Нужно написать код, который из Feb 12 2019 2:41PM сделает 2019-02-12 14:41:00
"""
from datetime import datetime

datetime_string = "Feb 12 2019 2:41PM"
datetime_obj = datetime.strptime(datetime_string, '%b %d %Y %I:%M%p')
print(datetime_obj)
"""
3. Напишите программу, которая принимает год, и возвращает список дат всех понедельников в данном году.
   Работа с датами (можно использовать любые модули и гуглить)
"""
import calendar
from datetime import date


def yerr(year):
    data = []
    for month in range(1, 13):
        for weak in calendar.Calendar().monthdatescalendar(year=year, month=month):
            for day in weak:
                if day.isoweekday() == 1 and day.year == year:
                    data.append(day.strftime('%d-%m-%Y'))
    return data

print(yerr(3020))