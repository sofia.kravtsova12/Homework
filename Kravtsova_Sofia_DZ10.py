"""
Дана функция, она не рабочая. Нужно описать что тут не так текстом, поправить ошибки, словом -
сделать что бы работало.
К примеру:
 - console.log('fff') # в питоне такого нет (меняем но то что есть в питоне) -> print('fff')
"""

# import 'os'                                -> убираем ''
# func readFileId(names=[], mode):           -> в питоне нет func меняем на def, убираем все из скобочек
#     _ = ''                                 -> переносим names=[] в эту строку
#     id = -1
#     for n in names:
#         with os.open(n, 'w') as f:         -> добавляем open()
#             _ += f.read()
#         f.close()                          -> убираем f.close() т.к есть open()
#     print 'default: ' + id + ', actual: ' + _ -> добавляем после print () и убираем + и ставим f перед ''
#     return id ? _ : id                        -> меняем ? на and и : на ==

import os


def readFileId():
    names = []
    _ = ''
    id = -1
    for n in names:
        with open(os.open(n, 'w')) as f:
            _ += f.read()
    print(f'default:{id} actual:{_}')
    return id and _ == id

readFileId()