"""
1. Валидатор паролей. На занятии делали валидацию емейла, тут
    нужно сделать валидацию пароля по такому же принципу.
    Придумать критаерии надежного пароля, описать их в условиях.
"""


password = input('Enter your password:')
if len(password) < 8:
    print('Invalid')
elif password.isalnum() and password.istitle():
    print('Valid')
else:
    print('Invalid')
"""
2. Создать список, где все элементы будут кратные 5ти (упражнение на функцию range)
"""
my_list = [i for i in range(100) if not i % 5]
print(my_list)
"""
3. Нарисовать в консоли ёлочку)
"""
def console_picture():
    print("           /\      ")
    print("          /  \     ")
    print("         /    \    ")
    print("        /      \   ")
    print("       /        \  ")
    print("      /          \ ")
    print("      ````|_|````` ")

console_picture()
"""
4. Задать число (input или number=Ваше число) и посчитать количество цифр в нем
"""

number = input('Enter your number:')
if number.isdigit():
    print(f'It"s count {len(number)}')
else:
    print("It's not a number")
"""
5. Сгенерировать произваольный список и развернуть его
"""
spisok = (1, 3, 6, 8, 90)
#for x in spisok:
    #print(x)
print(spisok[::-1])

def reverse(ist):
    return {v: k for k, v in ist.items()}
print(reverse({1: 2, 3: 4}))