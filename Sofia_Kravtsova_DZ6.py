"""
1. Программа должна запрашивать у пользователя сумму в гривнах (uah), и тип валюты (usd/euro) для обмена.
Если валюта не из списка, печатаем ошибку.
Если все ок, печатаем сумму в запрашиваемой валюте по курсу.
"""


def converter(summ:float, received:str)->float:
    valutec = {'usd': 25, 'eur': 30}
    if not valutec[received]:
        print('Choose another valute')
    else:
        return summ * valutec[received]

print(converter(55, 'usd'))

"""
2. Написать функцию, которая на вход принимает число и возвращает сумму всех его цифр. 
Операцию повторять до тех пор, пока не останется одна цифра.
Например:
дано: 5349
5 + 3 + 4 + 9 = 21 2 + 1= 3
вывод: 3
"""


def n_number(n: int) -> int:
    summ = 0
    while n > 0:
        summ += n % 10
        n = n // 10
        return summ

print(n_number(646464))

"""
3. Написать функцию для сортировки для  списка словарей.
Сортировать по ключу `name`, если такого ключа нету в словаре, то по ключу `lastname`
Пример словаря - {'name': 'Ivan', 'lastname': 'Ivanov'}
"""

l_names = {'name': 'Ivan', 'lastname': 'Vanov',
    'name1': 'Tonia', 'lastname1': 'Panova',
    'name2': 'Dave', 'lastname2': 'Strider'}
list_keys = list(l_names.keys())
list_keys.sort()
for i in list_keys:
    print(i, ':', l_names[i])