"""
Необходимо реализовать игру крестики-нолики на классах
 - 2 игрока
 - выбор кто ходит первый - рандом
"""
import random
import typing as t

class User(t.NamedTuple):
    name: str
    sign: str


class XO:
    __ALLOWED_SINGS = {'X', '0'}
    __EMPTY = '_'

    def __init__(self):
        self.table = [
            ['_', '_', '_'],
            ['_', '_', '_'],
            ['_', '_', '_'],
        ]
        self.table_map = {
            '1': [0, 0],
            '2': [0, 1],
            '3': [0, -1],
            '4': [1, 0],
            '5': [1, 1],
            '6': [1, -1],
            '7': [-1, 0],
            '8': [-1, 1],
            '9': [-1, -1],
        }

        self.winner_coords = (
            {'1', '2', '3'},
            {'4', '5', '6'},
            {'7', '8', '9'},
            {'1', '4', '7'},
            {'2', '5', '8'},
            {'3', '6', '9'},
            {'1', '5', '9'},
            {'3', '5', '7'},
        )

        self.x_coords = set()
        self.o_coords = set()

    def make_mark(self, field_num, sign):
        if sign not in self.__ALLOWED_SINGS:
            return
        coords = self.table_map[field_num]
        existing_sign = self.table[coords[0]][coords[-1]]
        if existing_sign != self.__EMPTY:
            print(f'Point already allocated with {existing_sign}')
            return
        self.table[coords[0]][coords[-1]] = sign
        if sign == 'X':
            self.x_coords.add(field_num)
        else:
            self.o_coords.add(field_num)

    def is_end(self, user_sing):
        for coord in self.winner_coords:
            if user_sing == 'X':
                if coord.issubset(self.x_coords):
                    return True
            else:
                if coord.issubset(self.o_coords):
                    return True

    def __str__(self):
        return f"{self.table[0][0]}\t | \t{self.table[0][1]}\t | \t{self.table[0][-1]}\n" \
               f"{self.table[1][0]}\t | \t{self.table[1][1]}\t | \t{self.table[1][-1]}\n" \
               f"{self.table[-1][0]}\t | \t{self.table[-1][1]}\t | \t{self.table[-1][-1]}\n"



def main():
    xo = XO()
    first_user_name = input('Input your name')
    second_user_name = input('Input your name')
    user_list = [first_user_name, second_user_name]
    x_user = random.choice(user_list)
    print(f'First user name {x_user}')
    user_list.pop(user_list.index(x_user))
    users = (User(name=x_user, sign='X'), User(name=user_list[0], sign='0'))
    is_first = False
    count = 0
    while True:
        print(xo)
        user = users[is_first]
        print(f'Current user {user.name}')
        number_of_point = input('Point number')
        xo.make_mark(field_num=number_of_point, sign=user.sign)
        count += 1
        if count >= 3 and xo.is_end(user.sign):
            print(f'{user.name} if winner')
            print(xo)
            return
        is_first = not is_first


main()

