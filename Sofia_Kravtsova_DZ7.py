"""
Написать игру "Угадай число". Написать программу, которая "загадает" рандомное число от 1 до n.
    Есть 2 (и больше) учасников, которые должны отгадать это число, если число отгадано - выводить
    имя участника и попытку, с которой он отгадал число.
Правила:
 - писать все функциями
 - обязательно запускать код для проверки!
 - число загадывается 1 раз на все время исполнения программы
 - количество попыток устанавливать сомостоятельно
"""
import random

suggests_made = 0
name = input('Who are you?')
x = random.randint(1, 10)
print('Fine, {0}, my number is between 1 and 10.'.format(name))

while suggests_made != x:
    suggest = int(input('Write some number: '))
    suggests_made += 1

    if suggest < x:
        print('Your number is lower than mine.')

    if suggest > x:
        print('Your number is higher than mine.')

    if suggest == x:
        break

if suggest == x:
    print('Ok, {0}. This is correct number.Tries = {1}.'.format(name, suggests_made))
else:
    print('Wrong. My number is {0}.'.format(x))