"""
1. Написать любую детерменированную функцию (Детерменированная функция = функция, которая возвращает одно и тоже вне
зависимости от парамеметров)
"""
def rr_sum(c, d):
    return c + d
"""
2. Написать функцию, которая вернет True если число четное и False если не четное
"""

def main():
    dgt = int(input('Input number:'))
    if not dgt % 2 or dgt == 0:
        result = True
    else:
        result = False
    return result


print(main())

def is_digt(number):
    return number % 2 == 0


print(is_digt(6))
"""
3. Напишите функция is_prime, которая принимает 1 аргумент (число) и возвращает True, если число простое, иначе False
Простое число - это число, которое делится без остатка только на себя и на 1
"""

def is_prime(number):
    if number > 1:
        for i in range(2, int(number / 2) + 1):
            if number % i == 0:
                return False
    return True

print(is_prime(7))
"""
4. Напишите функцию, которая принимает 1 аргумент (строка) и выполняет следующие действия на каждую из букв строки:
i - инкремент (+1)
d - дикремент (-1)
s - возведение в квадрат
o - добавить число в результативный список
остальные буквы игнорируются
Исходное число = 0
Результативный список = []
Вернуть результативный список
parse("iiisdoso")  ==>  [8, 64] <- это как пример
"""
def ppr(oper):
    start =0
    result =[]
    for opp in oper:
        if opp == 'o':
            result.append(start)
        elif opp == 'i':
            start += 1
        elif opp == 'd':
            start -= 1
        elif opp == 's':
            start **= 2

    return result
print(ppr('sssiidooss'))
"""
5. Написать функцию, которая из строки делает datetime и возвращает результат, если введенный 
формат даты неверный - возвращать None. Аргументы - срока-дата, формат, 
по которому можно привести строку в datetime
"""
from datetime import datetime


def date(date_str, format_str):
    try:
        return  datetime.strftime(date_str, format_str)
    except:
        return None

print(date('12.10.2022', '%h'))